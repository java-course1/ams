package hrd.controllers;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.layout.BorderPane;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class MainController implements Initializable {

    @FXML private BorderPane mainLayout;
    @FXML private Button btnArticle;
    @FXML private Button btnUser;

    public void loadCenterScene () throws IOException {
        Parent parent = FXMLLoader.load(getClass().getResource("../views/user.fxml"));
//        Stage stage = new Stage(new Scene());

        mainLayout.setCenter(parent);
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        try {
            loadCenterScene();
        } catch (IOException e) {
            e.printStackTrace();
        }

        btnArticle.setOnAction(new EventHandler<javafx.event.ActionEvent>() {
            @Override
            public void handle(javafx.event.ActionEvent event) {
                Parent parent = null;
                try {
                    parent = FXMLLoader.load(getClass().getResource("../views/article.fxml"));
                } catch (IOException e) {
                    e.printStackTrace();
                }
                mainLayout.setCenter(parent);
            }
        });

        btnUser.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Parent parent = null;
                try {
                    parent = FXMLLoader.load(getClass().getResource("../views/user.fxml"));
                } catch (IOException e) {
                    e.printStackTrace();
                }
                mainLayout.setCenter(parent);
            }
        });
    }
}
