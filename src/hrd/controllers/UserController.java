package hrd.controllers;

import hrd.models.dto.User;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.paint.Color;
import java.net.URL;
import java.util.Date;
import java.util.ResourceBundle;

public class UserController implements Initializable {

    @FXML private TextField txtFullName;
    @FXML private TextField txtContact;
    @FXML private RadioButton rdMale;
    @FXML private RadioButton rdFemale;
    @FXML private DatePicker birthDate;
    @FXML private TableView tbUser;
    @FXML private TableColumn colId;
    @FXML private TableColumn colName;
    @FXML private TableColumn colGender;
    @FXML private TableColumn colBirthDate;
    @FXML private TableColumn colContact;
    @FXML private ComboBox comboBox;
    @FXML private CheckBox cbNovel;
    @FXML private CheckBox cbLoveStory;
    @FXML private CheckBox cbFairyTale;
    @FXML private Label resultCbFavorite;
    @FXML private ColorPicker colorPicker;

    private ToggleGroup tgGender;


    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        tgGender = new ToggleGroup();
        rdMale.setToggleGroup(tgGender);
        rdFemale.setToggleGroup(tgGender);

        rdMale.setSelected(true);

        colId.setCellValueFactory(new PropertyValueFactory<>("id"));
        colName.setCellValueFactory(new PropertyValueFactory<>("name"));
        colGender.setCellValueFactory(new PropertyValueFactory<>("gender"));
        colBirthDate.setCellValueFactory(new PropertyValueFactory<>("birthDate"));
        colContact.setCellValueFactory(new PropertyValueFactory<>("contact"));

        comboBox.getItems().add("Option 1");
        comboBox.getItems().add("Option 2");
        comboBox.getItems().add("Option 3");
        comboBox.getItems().add("Option 4");
    }


    @FXML private void actionBtnSaveClicked (ActionEvent event) {
        User user = new User();
        user.setId(new Date().toString());
        user.setName(txtFullName.getText().trim());
        user.setContact(txtContact.getText());
        user.setBirthDate(birthDate.getValue());

        if (rdMale.isSelected()) {
            user.setGender("Male");
        } else if (rdFemale.isSelected()) {
            user.setGender("Female");
        }


        // value from checkbox
        String valueFromCheckBox = "Result from Checkbox : ";
        if (cbNovel.isSelected()) {
            valueFromCheckBox += cbNovel.getText();
        }
        if (cbLoveStory.isSelected()) {
            valueFromCheckBox += cbLoveStory.getText();
        }
        if (cbFairyTale.isSelected()) {
            valueFromCheckBox += cbFairyTale.getText();
        }

        resultCbFavorite.setText(valueFromCheckBox);


        // color picker
        Color valueColorPicker = colorPicker.getValue();
        resultCbFavorite.setTextFill(valueColorPicker);
        System.out.println(valueColorPicker.toString());

        // combo box
        Integer index = comboBox.getSelectionModel().getSelectedIndex();
        Object itemSelected = comboBox.getSelectionModel().getSelectedItem();

        System.out.println(index + "| " + itemSelected);

        if (index != -1 || itemSelected != null) {
            resultCbFavorite.setText(itemSelected.toString());
            System.out.println("index : " + index + "\t" + itemSelected);
        }


        tbUser.getItems().add(user);

        System.out.println(user);
    }


}
